using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Infrastructure.Implements.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EventManagerWeb.Pages.HomePage
{
    public class IndexModel : PageModel
    {
        private readonly IUnitOfWork _unitOfWork;

        public IndexModel(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public List<Event> Events { get; set; }
        public async Task OnGetAsync()
        {
            List<Event> ev = await _unitOfWork.EventRepository.GetEventsCheckDeleteAsync();
            Events = ev.ToList();
			
		}
    }
}
