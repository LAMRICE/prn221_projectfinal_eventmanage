using Application.Commons;
using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EventManagerWeb.Pages.HomePage
{
    public class ExploreModel : PageModel
    {
        private readonly IUnitOfWork _unitOfWork;
        public ExploreModel(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
		[BindProperty]
		public List<EventCategory> EventCategories { get; set; }
		public List<Event> Events { get; set; }
		public async Task OnGetAsync()
        {

            Events = await _unitOfWork.EventRepository.GetEventsCheckDeleteAsync();
			EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

		}

		public async Task<IActionResult> OnPostSearchByCategoryAsync(Guid categoryId)
		{
			if (categoryId != null)
			{
				Events = await _unitOfWork.EventRepository.GetEventsByCategoryIdAsync(categoryId);

			}
			else
			{
				// N?u categoryId kh�ng h?p l?, hi?n th? t?t c? s? ki?n
				Events = await _unitOfWork.EventRepository.GetEventsCheckDeleteAsync();

			}

			// L?y l?i danh s�ch c�c category
			EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();


			// Sau khi l?c s? ki?n theo category, tr? l?i trang explore
			return Page();
		}

		public async Task<IActionResult> OnPostSearchAsync(string searchText)
		{
			if (!string.IsNullOrEmpty(searchText))
			{
				// G?i ph??ng th?c ho?c truy v?n t? IUnitOfWork ?? l?c danh s�ch s? ki?n d?a tr�n t�n s? ki?n ?� nh?p
				Events = await _unitOfWork.EventRepository.SearchEventsByEventName(searchText);
				EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();
			}
			else
			{
				// N?u kh�ng c� gi� tr? t�m ki?m, hi?n th? t?t c? s? ki?n
				Events = await _unitOfWork.EventRepository.GetEventsCheckDeleteAsync();
				EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();
			}

			// C?p nh?t danh s�ch s? ki?n trong ExploreModel v� tr? v? trang web
			return Page();
		}

		public async Task<IActionResult> OnPostSearchByLocationAsync(string location)
		{
			if (!string.IsNullOrEmpty(location))
			{
				// G?i ph??ng th?c ho?c truy v?n t? IUnitOfWork ?? l?c danh s�ch s? ki?n d?a tr�n ??a ?i?m ?� nh?p
				Events = await _unitOfWork.EventRepository.SearchEventsByLocation(location);
				EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

			}
			else
			{
				// N?u kh�ng c� gi� tr? ??a ?i?m, hi?n th? t?t c? s? ki?n
				Events = await _unitOfWork.EventRepository.GetEventsCheckDeleteAsync();
				EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

			}

			// C?p nh?t danh s�ch s? ki?n trong ExploreModel v� tr? v? trang web
			return Page();
		}

		public async Task<IActionResult> OnPostSearchByDateAsync(string dateRadioDefault)
		{
			switch (dateRadioDefault)
			{
				case "all":
					Events = await _unitOfWork.EventRepository.GetEventsCheckDeleteAsync();
					EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

					break;
				case "15":
					Events = await _unitOfWork.EventRepository.GetEventsWithinDaysAsync(15);
					EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

					break;
				case "10":
					Events = await _unitOfWork.EventRepository.GetEventsWithinDaysAsync(10);
					EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

					break;
				case "5":
					Events = await _unitOfWork.EventRepository.GetEventsWithinDaysAsync(5);
					EventCategories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

					break;
			}

			return Page();
		}
	}
}
