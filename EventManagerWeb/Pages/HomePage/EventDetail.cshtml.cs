using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EventManagerWeb.Pages.HomePage
{
    public class EventDetailModel : PageModel
    {
        private readonly IUnitOfWork _unitOfWork;
        public EventDetailModel(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public Guid Id { get; set; }
        public Event EventVm { get; set; }
        public async Task OnGetAsync(Guid idE)
        {
            Id = idE;
            Event ev = await _unitOfWork.EventRepository.GetEventByUserIdAsync(idE);
            EventVm = ev;
        }
    }
}
