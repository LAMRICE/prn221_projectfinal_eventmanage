﻿using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EventManagerWeb.Pages.EventManage
{
    public class UserProfileModel : PageModel
    {
		private readonly IWebHostEnvironment _environment;
		private readonly UserManager<AppUser> _userManager;
		private readonly SignInManager<AppUser> _signInManager;
		private readonly IUnitOfWork _unitOfWork;
		public UserProfileModel(IWebHostEnvironment environment, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IUnitOfWork unitOfWork)
		{
			_userManager = userManager;
			_signInManager = signInManager;
			_environment = environment;
			_unitOfWork = unitOfWork;
		}
		[BindProperty]
		public AppUser user { get; set; }
		public IFormFile Upload { get; set; }

		public async Task OnGetAsync()
        {
			if(_signInManager.IsSignedIn(User))
			{
				user = await _userManager.GetUserAsync(User);			
			}			
		}

		public async Task<IActionResult> OnPostAsync()
		{
			if (!ModelState.IsValid)
			{
				return Page();
			}

			if (_signInManager.IsSignedIn(User))
			{
				var timestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
				var fileName = $"{timestamp}_{Path.GetFileName(Upload.FileName)}";
				var filePath = Path.Combine(_environment.WebRootPath, "asset\\upload", fileName);

				using (var stream = System.IO.File.Create(filePath))
				{
					await Upload.CopyToAsync(stream);
				}
			}

			

			// Lưu đường dẫn của file vào database nếu cần

			return Page();
		}

	}
}
