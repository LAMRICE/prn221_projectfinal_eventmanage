﻿using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;

namespace EventManagerWeb.Pages.EventManage
{
    public class AddEventModel : PageModel
    {
        private readonly IUnitOfWork _unitOfWork;
		private readonly IWebHostEnvironment _environment;
		private readonly UserManager<AppUser> _userManager;

		public AddEventModel(IUnitOfWork unitOfWork, IWebHostEnvironment environment, UserManager<AppUser> userManager)
        {
            _unitOfWork = unitOfWork;
			_environment = environment;
			_userManager = userManager;
        }

		[BindProperty]
		public Event Event { get; set; }
		public IFormFile Upload { get; set; }

		public List<EventCategory> EventCategories { get; set; }

		
		public async Task OnGetAsync()
		{
			var categories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

			EventCategories = new List<EventCategory>();

			// Thêm từng category vào danh sách
			foreach (var category in categories)
			{
				EventCategories.Add(new EventCategory { Id = category.Id, CategoryName = category.CategoryName, CategoryDescription=category.CategoryDescription });
			}
		}

		public async Task<IActionResult> OnPostAsync()
        {
			var user = await _userManager.GetUserAsync(User);

			var timestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
			var fileName = $"{timestamp}_{Path.GetFileName(Upload.FileName)}";
			var filePath = Path.Combine(_environment.WebRootPath, "asset\\upload\\events", fileName);

			using (var stream = System.IO.File.Create(filePath))
			{
				await Upload.CopyToAsync(stream);
			}
			Event.UserId = user?.Id;
			Event.UrlImage = "~/asset/upload/events/" + fileName;
			var entry = Event;
			await _unitOfWork.EventRepository.AddAsync(entry);
			return Redirect("/");	
		}
    }
}
