using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EventManagerWeb.Pages.EventManage
{
    public class ListEventModel : PageModel
    {
		private readonly IUnitOfWork _unitOfWork;
		
		private readonly UserManager<AppUser> _userManager;

		public ListEventModel(IUnitOfWork unitOfWork, UserManager<AppUser> userManager)
		{
			_unitOfWork = unitOfWork;
			_userManager = userManager;
		}

		[BindProperty]
		public List<Event> Events { get; set; }
		public async Task OnGetAsync()
		{
			var user = await _userManager.GetUserAsync(User);
			Events = await _unitOfWork.EventRepository.GetEventsByUserAsync(user.Id);
        }

		public async Task<IActionResult> OnPostDeleteAsync(Guid eventId)
		{
			var eventToDelete = await _unitOfWork.EventRepository.GetByIdAsync(eventId);
			if (eventToDelete == null)
			{
				// S? ki?n kh�ng t?n t?i.
				return Page();
			}
			eventToDelete.IsDelete = true;
			await _unitOfWork.EventRepository.UpdateAsync(eventToDelete);

			return RedirectToPage(); // Quay tr? l?i trang danh s�ch sau khi x�a.
		}
	}
}
