using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EventManagerWeb.Pages.EventManage
{
    public class CheckoutModel : PageModel
    {
		private readonly IUnitOfWork _unitOfWork;
		public CheckoutModel(IUnitOfWork unitOfWork)
		{
			_unitOfWork = unitOfWork;
		}

		public Event Event { get; set; }
		public async Task OnGetAsync(Guid idE)
		{
			Event = await _unitOfWork.EventRepository.GetEventInclueAsync(idE);
		}
	}
}
