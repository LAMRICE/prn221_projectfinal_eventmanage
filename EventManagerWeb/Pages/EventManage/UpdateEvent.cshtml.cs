﻿using Application.Contracts.IUnitOfWork;
using Domain.Entities;
using Infrastructure.Data;
using Infrastructure.Implements.UnitOfWork;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace EventManagerWeb.Pages.EventManage
{
    public class UpdateEventModel : PageModel
    {
        private readonly IUnitOfWork _unitOfWork;
		private readonly UserManager<AppUser> _userManager;
		private readonly IWebHostEnvironment _environment;
		private readonly EventManagerDbContext _eventManagerContext;

		public UpdateEventModel(IUnitOfWork unitOfWork, UserManager<AppUser> userManager, IWebHostEnvironment environment, EventManagerDbContext eventManagerContext)
        {
            _unitOfWork = unitOfWork;
			_userManager = userManager;
			_environment = environment;
			_eventManagerContext = eventManagerContext;
        }
		[BindProperty(SupportsGet = true)]
		public Guid EventId { get; set; }

		
		[BindProperty]
		public Event Event { get; set; }
		public List<EventCategory> EventCategories { get; set; }

		public IFormFile Upload { get; set; }
		public async Task OnGetAsync()
		{
			var categories = await _unitOfWork.EventCategoryRepository.GetAllAsync();

			EventCategories = new List<EventCategory>();

			// Thêm từng category vào danh sách
			foreach (var category in categories)
			{
				EventCategories.Add(new EventCategory { Id = category.Id, CategoryName = category.CategoryName, CategoryDescription = category.CategoryDescription });
			}


			Event = await _unitOfWork.EventRepository.GetByIdAsync(EventId);

			
		}

		public async Task<IActionResult> OnPostAsync()
		{

			var timestamp = DateTime.Now.ToString("yyyyMMddHHmmssfff");
			var fileName = $"{timestamp}_{Path.GetFileName(Upload.FileName)}";
			var filePath = Path.Combine(_environment.WebRootPath, "asset\\upload\\events", fileName);

			using (var stream = System.IO.File.Create(filePath))
			{
				await Upload.CopyToAsync(stream);
			}
			var entry = Event;
			entry.UrlImage = "~/asset/upload/events/" + fileName;
			Event events = await _unitOfWork.EventRepository.GetByIdAsync(EventId);
			events.EventName = entry.EventName;
			events.EventCategory = entry.EventCategory;
			events.DateBegin = entry.DateBegin;
			events.DateEnd = entry.DateEnd;
			events.EventLocation = entry.EventLocation;
			events.EventDescription	= entry.EventDescription;
			_eventManagerContext.Attach(events).State = EntityState.Modified;
			_eventManagerContext.SaveChanges();

			//await _unitOfWork.EventRepository.UpdateAsync(entry);
			return Redirect("/");
		}


	}
}
