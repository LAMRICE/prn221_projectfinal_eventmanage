using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
//using EventManagerWeb.Data;
using Domain.Entities;
using EventManagerWeb.Data;
using Infrastructure;
using Infrastructure.Data;
using Infrastructure.Implements.UnitOfWork;
using Application.Contracts.IRepository;
using Infrastructure.Implements.Repository;


var builder = WebApplication.CreateBuilder(args);
var connectionString = builder.Configuration.GetConnectionString("DefaultDB") ?? throw new InvalidOperationException("Connection string 'EventManagerWebContextConnection' not found.");


builder.Services.AddDbContext<EventManagerDbContext>(options => options.UseSqlServer(connectionString));

builder.Services.AddDefaultIdentity<AppUser>(options => options.SignIn.RequireConfirmedAccount = false).AddEntityFrameworkStores<EventManagerDbContext>();
builder.Services.AddScoped<Application.Contracts.IUnitOfWork.IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<IAppUserRepository, AppUserRepository>();
builder.Services.AddScoped<IEventRepository, EventRepository>();
builder.Services.AddScoped<IEventCategoryRepository, EventCategoryRepository>();
builder.Services.AddScoped<IBookingRepository, BookingRepository>();
builder.Services.AddScoped<IFeedbackRepository, FeedbackRepository>();
builder.Services.AddScoped<INotificationRepository, NotificationRepository>();
builder.Services.AddScoped<IPaymentRepository, PaymentRepository>();
builder.Services.AddScoped<ITicketRepository, TicketRepository>();
builder.Services.AddScoped<IPromotionRepository, PromotionRepository>();




builder.Services.Configure<IdentityOptions>(options =>
{
    //Config Password
    options.Password.RequireDigit = false;
    options.Password.RequireLowercase = false;
    options.Password.RequireUppercase = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequiredLength = 6;

    options.Lockout.AllowedForNewUsers = false;
    options.Lockout.MaxFailedAccessAttempts = 10;

    //username
    options.User.RequireUniqueEmail = true;

    //dang nhap:
    options.SignIn.RequireConfirmedEmail = false;
});



// Add services to the container.
builder.Services.AddRazorPages();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
