﻿using Application.Contracts.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Contracts.IUnitOfWork
{
	public interface IUnitOfWork
	{
		public IAppUserRepository AppUserRepository { get; }
		public IBookingRepository BookingRepository { get; }
		public IEventCategoryRepository EventCategoryRepository { get; }
		public IEventRepository EventRepository { get; }
		public IFeedbackRepository FeedbackRepository { get; }
		public INotificationRepository NotificationRepository { get; }
		public IPaymentRepository PaymentRepository { get; }
		public IPromotionRepository PromotionRepository { get; }
		public ITicketRepository TicketRepository { get; }
	}
}
