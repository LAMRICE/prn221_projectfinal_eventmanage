﻿using Application.Commons;
using Domain.Entities;

namespace Application.Contracts.IRepository
{
	public interface INotificationRepository:IGenericRepository<Notification>
	{
		Task<Pagination<Notification>> GetAllNotification(Guid userId, int pageIndex, int pageSize);
	}
}
