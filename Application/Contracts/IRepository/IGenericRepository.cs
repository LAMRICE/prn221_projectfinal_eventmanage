﻿using Application.Commons;
using Domain.Entities;
using System.Linq.Expressions;

namespace Application.Contracts.IRepository
{
	public interface IGenericRepository<T> where T : BaseEntity
	{
		Task<T?> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includes);
		Task<List<T>> GetAllAsync();
		Task AddAsync(T entity);
		Task UpdateAsync(T entity);
		Task RemoveAsyncId(Guid id);
		Task<bool> ExistsAsync(Guid id);
		Task<int> CountAsync();
		Task SaveChangesAsync();
		Task<Pagination<T>> ToPagination(int pageIndex, int pagesize);

		Task AddRangeAsync(IEnumerable<T> entities);
		Task UpdateAsync2(T entity);
	}
}
