﻿using Domain.Entities;

namespace Application.Contracts.IRepository
{
	public interface IPaymentRepository :IGenericRepository<Payment>
	{
		Task<Payment?> GetPaymentsByBookingAsync(Guid bookingId);
	}
}
