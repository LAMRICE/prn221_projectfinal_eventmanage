﻿using Application.Commons;
using Domain.Entities;

namespace Application.Contracts.IRepository
{
	public interface IFeedbackRepository :IGenericRepository<Feedback>
	{
		Task<List<Feedback>> GetFeedbacksByUserAsync(Guid userId);
		Task<List<Feedback>> GetFeedbacksByEventAsync(Guid eventId);
		Task<Pagination<Feedback>> GetEventsByEventPaginAsync(Guid eventId, int pageIndex, int pagesize);

	}
}
