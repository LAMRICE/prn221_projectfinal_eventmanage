﻿using Application.Commons;
using Domain.Entities;

namespace Application.Contracts.IRepository
{
	public interface IEventCategoryRepository : IGenericRepository<EventCategory>
	{
		
	}
}
