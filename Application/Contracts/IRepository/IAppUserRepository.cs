﻿using Application.Commons;
using Domain.Entities;
using System.Linq.Expressions;

namespace Application.Contracts.IRepository
{
	public interface IAppUserRepository 
	{
		Task<AppUser?> GetByIdAsync(Guid id, params Expression<Func<AppUser, object>>[] includes);
		Task<List<AppUser>> GetAllAsync();
		Task AddAsync(AppUser entity);
		Task UpdateAsync(AppUser entity);
		Task RemoveAsync(AppUser entity);
		Task RemoveAsyncId(Guid id);
		Task<bool> ExistsAsync(Guid id);
		Task<int> CountAsync();
		Task SaveChangesAsync();
		Task<Pagination<AppUser>> ToPagination(int pageIndex, int pageSize);
		Task<AppUser?> GetUserByUserNameAsync(string username);
	}
}
