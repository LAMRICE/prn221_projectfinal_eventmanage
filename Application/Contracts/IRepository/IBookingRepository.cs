﻿using Application.Commons;
using Domain.Entities;

namespace Application.Contracts.IRepository
{
	public interface IBookingRepository: IGenericRepository<Booking>
	{
		Task<List<Booking>> GetBookingsByUserAsync(Guid? userId);
	
		Task<Pagination<Booking>> GetBookingsByUserPaginAsync(Guid userId, int pageIndex, int pagesize);
	}
}
