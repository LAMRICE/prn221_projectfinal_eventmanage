﻿using Application.Commons;
using Domain.Entities;

namespace Application.Contracts.IRepository
{
	public interface IEventRepository : IGenericRepository<Event>
	{
		
		Task<List<Event>> GetEventsByUserAsync(string userId);
		Task<Pagination<Event>> GetEventsByUserPaginAsync(Guid userId,int pageIndex, int pagesize);
		Task<Pagination<Event>> FillterEventPaginAsync(string searchValue, int pageIndex, int pagesize);
		 Task<List<Event>> GetEventsCheckDeleteAsync();
		Task<Event> GetEventByUserIdAsync(Guid evID);

		Task<List<Event>> SearchEventsByEventName(string searchText);
		Task<List<Event>> SearchEventsByLocation(string location);
		Task<List<Event>> GetEventsWithinDaysAsync(int days);
		Task<List<Event>> GetEventsByCategoryIdAsync(Guid categoryId);
		Task<Event> GetEventInclueAsync(Guid evID);
	}
}
