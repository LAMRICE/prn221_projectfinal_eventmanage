﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.Contracts.IRepository
{
	public interface ITicketRepository : IGenericRepository<Ticket>
	{
		Task<List<Ticket>> GetTicketsByEventIdAsync(Guid eventId);
	}
}
