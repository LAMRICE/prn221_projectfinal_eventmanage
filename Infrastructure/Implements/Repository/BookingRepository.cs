﻿using Application.Commons;
using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements.Repository
{
	public class BookingRepository : GenericRepository<Booking>, IBookingRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public BookingRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task<List<Booking>> GetBookingsByUserAsync(Guid? userId)
		{
			var events = await _dbContext.Bookings
				.Include(e => e.AppUser) // Load thông tin của người dùng liên quan
				.Where(e => e.UserId == userId.ToString()) // Lọc sự kiện của người dùng cụ thể
				.ToListAsync();
			return events;
		}

		public async Task<Pagination<Booking>> GetBookingsByUserPaginAsync(Guid userId, int pageIndex, int pagesize)
		{
			var list = _dbContext.Bookings
				.Include(e => e.AppUser) // Load thông tin của người dùng liên quan
				.Where(e => e.UserId == userId.ToString()) // Lọc sự kiện của người dùng cụ thể
				.OrderByDescending(e => e.CreatedDate);
			var itemCount = await list.CountAsync();
			var bookings = await list.Skip(pageIndex * pagesize).Take(itemCount).ToListAsync();
			var pagination = new Pagination<Booking>()
			{
				TotalItemsCount = itemCount,
				PageSize = pagesize,
				PageIndex = pageIndex,
				Items = bookings
			};
			return pagination;
		}
	}
}
