﻿using Application.Commons;
using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Infrastructure.Implements.Repository
{
	public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
	{

		private readonly EventManagerDbContext _dbContext;
		public GenericRepository(EventManagerDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task AddAsync(T entity)
		{
			await _dbContext.Set<T>().AddAsync(entity);
			await _dbContext.SaveChangesAsync();

		}

		public async Task AddRangeAsync(IEnumerable<T> entities)
		{
			await _dbContext.Set<T>().AddRangeAsync(entities);
			await _dbContext.SaveChangesAsync();
		}

		public async Task<int> CountAsync()
		{
			return await _dbContext.Set<T>().CountAsync();
		}

		public async Task<bool> ExistsAsync(Guid id)
		{
			return await _dbContext.Set<T>().AnyAsync(e => e.Id == id);
		}

		public async Task<List<T>> GetAllAsync()
		{
			return await _dbContext.Set<T>()
				.OrderByDescending(x => x.CreatedDate)
				.ToListAsync();
		}

		public async Task<T?> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] includes)
		{
			return await includes.Aggregate(_dbContext.Set<T>().AsNoTracking(), (a, b) => a.Include(b)).FirstOrDefaultAsync(x => x.Id == id);
		}

		public async Task RemoveAsync(T entity)
		{
			_dbContext.Set<T>().Remove(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task RemoveAsyncId(Guid id)
		{
			var entity = await this.GetByIdAsync(id);
			if (entity == null) throw new InvalidOperationException("Id not found");
			await this.RemoveAsync(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task SaveChangesAsync()
		{
			await _dbContext.SaveChangesAsync();
		}

		public async Task<Pagination<T>> ToPagination(int pageIndex, int pagesize)
		{
			var itemCount = await _dbContext.Set<T>().CountAsync();
			var items = await _dbContext.Set<T>()
								.Skip(pageIndex * pagesize)
								.Take(pagesize)
								.AsNoTracking()
								.OrderByDescending(x => x.CreatedDate)
								.ToListAsync();
			return new Pagination<T>
			{
				Items = items,
				PageIndex = pageIndex,
				PageSize = pagesize,
				TotalItemsCount = itemCount
			};

		}
		public async Task UpdateAsync(T entity)
		{
			_dbContext.Entry<T>(entity).State = EntityState.Modified;
			await _dbContext.SaveChangesAsync();
		}

		public async Task UpdateAsync2(T entity)
		{
			var existingEntity = await _dbContext.Set<T>().FindAsync(entity.Id);

			if (existingEntity != null)
			{
				_dbContext.Entry(existingEntity).CurrentValues.SetValues(entity);
				await _dbContext.SaveChangesAsync();
			}
			else
			{
				// Xử lý khi không tìm thấy đối tượng cần cập nhật
				throw new Exception("Entity not found.");
			}
		}

	}
}
