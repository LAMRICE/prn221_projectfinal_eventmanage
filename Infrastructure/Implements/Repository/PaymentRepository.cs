﻿using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements.Repository
{
	public class PaymentRepository : GenericRepository<Payment>, IPaymentRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public PaymentRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task<Payment?> GetPaymentsByBookingAsync(Guid bookingId)
		{
			var payment = await _dbContext.Payments
				.Include(p => p.Booking) // Load thông tin đặt phòng liên quan
				.FirstOrDefaultAsync(p => p.BookingId == bookingId); // Lọc thanh toán theo ID của đặt phòng

			return payment;
		}
	}
}
