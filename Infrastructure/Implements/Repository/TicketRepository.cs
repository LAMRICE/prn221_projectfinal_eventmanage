﻿using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements.Repository
{
	public class TicketRepository : GenericRepository<Ticket>, ITicketRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public TicketRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		

		public async Task<List<Ticket>> GetTicketsByEventIdAsync(Guid eventId)
		{
			var tickets = await _dbContext.Tickets
			.Include(e => e.Event) // Load thông tin của người dùng liên quan
				.Where(e => e.EventId == eventId) // Lọc sự kiện của người dùng cụ thể
				.ToListAsync();
			return tickets;
		}
	}
}
