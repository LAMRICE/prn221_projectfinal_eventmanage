﻿using Application.Commons;
using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements.Repository
{
	public class FeedbackRepository : GenericRepository<Feedback>, IFeedbackRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public FeedbackRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task<Pagination<Feedback>> GetEventsByEventPaginAsync(Guid eventId, int pageIndex, int pagesize)
		{
			var list = _dbContext.Feedbacks
				.Include(e => e.Event) // Load thông tin của người dùng liên quan
				.Where(e => e.EventId == eventId) // Lọc sự kiện của người dùng cụ thể
				.OrderByDescending(e => e.CreatedDate);
			var itemCount = await list.CountAsync();
			var feedbacks = await list.Skip(pageIndex * pagesize).Take(itemCount).ToListAsync();
			var pagination = new Pagination<Feedback>()
			{
				TotalItemsCount = itemCount,
				PageSize = pagesize,
				PageIndex = pageIndex,
				Items = feedbacks
			};
			return pagination;
		}

		public async Task<List<Feedback>> GetFeedbacksByEventAsync(Guid eventId)
		{
			var feedbacks = await _dbContext.Feedbacks
				.Include(e => e.Event) // Load thông tin của người dùng liên quan
				.Where(e => e.EventId == eventId) // Lọc sự kiện của người dùng cụ thể
				.ToListAsync();
			return feedbacks;
		}

		public async Task<List<Feedback>> GetFeedbacksByUserAsync(Guid userId)
		{
			var feedbacks = await _dbContext.Feedbacks
				.Include(e => e.AppUser) // Load thông tin của người dùng liên quan
				.Where(e => e.UserId == userId.ToString()) // Lọc sự kiện của người dùng cụ thể
				.ToListAsync();
			return feedbacks;
		}
	}
}
