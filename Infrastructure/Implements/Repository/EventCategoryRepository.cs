﻿using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;

namespace Infrastructure.Implements.Repository
{
	public class EventCategoryRepository : GenericRepository<EventCategory>, IEventCategoryRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public EventCategoryRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

	}
}
