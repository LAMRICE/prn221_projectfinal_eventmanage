﻿using Application.Commons;
using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements.Repository
{
	public class EventRepository : GenericRepository<Event>, IEventRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public EventRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		public Task<Pagination<Event>> FillterEventPaginAsync(string searchValue, int pageIndex, int pagesize)
		{
			throw new NotImplementedException();
		}

		public async Task<Event> GetEventByUserIdAsync(Guid IdEv)
		{
			var ev = await _dbContext.Events.Include(x => x.AppUser).Include(x => x.Tickets).FirstOrDefaultAsync(x => x.Id == IdEv);
			return ev;
		}

		public async Task<Event> GetEventInclueAsync(Guid evID)
		{
			var ev = await _dbContext.Events.Include(x => x.Tickets).Include(x => x.Promotions).FirstOrDefaultAsync(x => x.Id == evID);
			return ev;
		}

		public async Task<List<Event>> GetEventsByUserAsync(string userId)
		{
			var events = await _dbContext.Events
				.Include(e => e.AppUser)
				.Include(e => e.EventCategory)
				.Include(e => e.Tickets)// Load thông tin của người dùng liên quan
				.Where(e => e.UserId == userId.ToString()) // Lọc sự kiện của người dùng cụ thể
				.Where(e => e.IsDelete == false)
				.ToListAsync();
			return events;
		}

		public async Task<List<Event>> GetEventsCheckDeleteAsync()
		{
			var events = await _dbContext.Events
				.Include(e => e.AppUser)
				.Include(e => e.EventCategory)
				.Include(e => e.Tickets)// Load thông tin của người dùng liên quan
				.Where(e => e.IsDelete == false)
				.ToListAsync();
			return events;
		}

		public async Task<Pagination<Event>> GetEventsByUserPaginAsync(Guid userId, int pageIndex, int pagesize)
		{
			var list =  _dbContext.Events
				.Include(e => e.AppUser) // Load thông tin của người dùng liên quan
				.Where(e => e.UserId == userId.ToString()) // Lọc sự kiện của người dùng cụ thể
				.OrderByDescending(e => e.CreatedDate);
			var itemCount = await list.CountAsync();
			var events = await list.Skip(pageIndex * pagesize).Take(itemCount).ToListAsync();
			var pagination = new Pagination<Event>()
			{
				TotalItemsCount = itemCount,
				PageSize = pagesize,
				PageIndex = pageIndex,
				Items = events
			};
			return pagination;
		}

		public async Task<List<Event>> GetEventsByCategoryIdAsync(Guid categoryId)
		{
			
			return await _dbContext.Events
				.Include(e => e.EventCategory)
				.Where(e => e.EventCategoryId == categoryId)
				.ToListAsync();
		}

		public async Task<List<Event>> SearchEventsByEventName(string searchText)
		{
			// Tìm kiếm sự kiện theo tên sự kiện
			return await _dbContext.Events
				.Include(e=>e.EventCategory)
				.Where(e => e.EventName.Contains(searchText))
				.ToListAsync();
		}

		public async Task<List<Event>> SearchEventsByLocation(string location)
		{
			return await _dbContext.Events
				.Include(e => e.EventCategory)
				.Where(e => e.EventLocation.Contains(location))
				.ToListAsync();
		}

		public async Task<List<Event>> GetEventsWithinDaysAsync(int days)
		{
			DateTime startDate = DateTime.Now.Date;
			DateTime endDate = startDate.AddDays(days);

			return await _dbContext.Events
				.Include(e => e.EventCategory)
				.Where(e => e.DateBegin >= startDate && e.DateBegin <= endDate)
				.ToListAsync();
		}
	}
}
