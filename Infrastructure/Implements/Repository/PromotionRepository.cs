﻿using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements.Repository
{
	public class PromotionRepository : GenericRepository<Promotion>, IPromotionRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public PromotionRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task<List<Promotion>> GetPromotionsByEventIdAsync(Guid eventId)
		{
			var promotions = await _dbContext.Promotions
				.Include(e => e.Event) // Load thông tin của người dùng liên quan
				.Where(e => e.EventId == eventId) // Lọc sự kiện của người dùng cụ thể
				.ToListAsync();
			return promotions;
		}
	}
}
