﻿using Application.Commons;
using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Implements.Repository
{
	public class NotificationRepository : GenericRepository<Notification>, INotificationRepository
	{
		private readonly EventManagerDbContext _dbContext;
		public NotificationRepository(EventManagerDbContext dbContext) : base(dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task<Pagination<Notification>> GetAllNotification(Guid userId, int pageIndex, int pageSize)
		{
			var list = _dbContext.Notifications
				.Include(e => e.AppUser) // Load thông tin của người dùng liên quan
			.Where(e => e.UserId == userId.ToString()) // Lọc sự kiện của người dùng cụ thể
				.OrderByDescending(e => e.CreatedDate);
			var itemCount = await list.CountAsync();
			var notifications = await list.Skip(pageIndex * pageSize).Take(itemCount).ToListAsync();
			var pagination = new Pagination<Notification>()
			{
				TotalItemsCount = itemCount,
				PageSize = pageSize,
				PageIndex = pageIndex,
				Items = notifications
			};
			return pagination;
		}
	}
}
