﻿using Application.Commons;
using Application.Contracts.IRepository;
using Domain.Entities;
using Infrastructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Infrastructure.Implements.Repository
{
	public class AppUserRepository : IAppUserRepository
	{
		private readonly EventManagerDbContext _dbContext;

		public AppUserRepository(EventManagerDbContext dbContext)
		{
			_dbContext = dbContext;
		}

		public async Task AddAsync(AppUser entity)
		{
			await _dbContext.Set<AppUser>().AddAsync(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task<int> CountAsync()
		{
			return await _dbContext.Set<AppUser>().CountAsync();
		}

		public async Task<bool> ExistsAsync(Guid id)
		{
			return await _dbContext.Set<AppUser>().AnyAsync(e => e.Id.Equals(id));
		}

		public async Task<List<AppUser>> GetAllAsync()
		{
			return await _dbContext.Set<AppUser>()
				.ToListAsync();
		}

		public async Task<AppUser?> GetByIdAsync(Guid id, params Expression<Func<AppUser, object>>[] includes)
		{
			return await includes.Aggregate(_dbContext.Set<AppUser>().AsNoTracking(), (a, b) => a.Include(b)).FirstOrDefaultAsync(x => x.Id.Equals(id));
		}

		public async Task<AppUser?> GetUserByUserNameAsync(string username)
		{
			return await _dbContext.Set<AppUser>()
							.FirstOrDefaultAsync(u => u.UserName.Equals(username));
		}



		public async Task RemoveAsync(AppUser entity)
		{
			_dbContext.Set<AppUser>().Remove(entity);
			await _dbContext.SaveChangesAsync();
		}

		public async Task RemoveAsyncId(Guid id)
		{
			var entity = await this.GetByIdAsync(id);
			if (entity == null) throw new InvalidOperationException("Id not found");
			await this.RemoveAsync(entity);
		}

		public async Task SaveChangesAsync()
		{
			await _dbContext.SaveChangesAsync();

		}

		public async Task<Pagination<AppUser>> ToPagination(int pageIndex, int pageSize)
		{
			var itemCount = await _dbContext.Set<AppUser>().CountAsync();
			var items = await _dbContext.Set<AppUser>()
								.Skip(pageIndex * pageSize)
								.Take(pageSize)
								.AsNoTracking()
								.ToListAsync();
			return new Pagination<AppUser>
			{
				Items = items,
				PageIndex = pageIndex,
				PageSize = pageSize,
				TotalItemsCount = itemCount
			};
		}

		public async Task UpdateAsync(AppUser entity)
		{
			_dbContext.Entry<AppUser>(entity).State = EntityState.Modified;
			await _dbContext.SaveChangesAsync();
		}
	}
}
