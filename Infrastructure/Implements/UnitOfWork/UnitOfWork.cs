﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.Contracts.IRepository;
using Application.Contracts.IUnitOfWork;
using Infrastructure.Data;

namespace Infrastructure.Implements.UnitOfWork
{
	public class UnitOfWork : IUnitOfWork
	{
		#region Fields
		private readonly EventManagerDbContext _dbContext;
		private readonly IAppUserRepository _appUserRepository;
		private readonly IEventCategoryRepository _eventCategoryRepository;
		private readonly IEventRepository _eventRepository;
		private readonly IBookingRepository _bookingRepository;
		private readonly IFeedbackRepository _feedbackRepository;
		private readonly INotificationRepository _notificationRepository;
		private readonly IPaymentRepository _paymentRepository;
		private readonly IPromotionRepository _promotionRepository;
		private readonly ITicketRepository _ticketRepository;
		#endregion

		public UnitOfWork(EventManagerDbContext dbContext,
			IAppUserRepository appUserRepository,
			IEventCategoryRepository eventCategoryRepository,
			IEventRepository eventRepository,
			IBookingRepository bookingRepository,
			IFeedbackRepository feedbackRepository,
			INotificationRepository notificationRepository,
			IPaymentRepository paymentRepository,
			IPromotionRepository promotionRepository,
			ITicketRepository ticketRepository)
		{
			_dbContext = dbContext;
			_appUserRepository = appUserRepository;
			_eventCategoryRepository = eventCategoryRepository;
			_eventRepository = eventRepository;
			_bookingRepository = bookingRepository;
			_feedbackRepository = feedbackRepository;
			_notificationRepository = notificationRepository;
			_paymentRepository = paymentRepository;
			_promotionRepository = promotionRepository;
			_ticketRepository = ticketRepository;
		}

		public IAppUserRepository AppUserRepository => _appUserRepository;

		public IBookingRepository BookingRepository => _bookingRepository;

		public IEventCategoryRepository EventCategoryRepository => _eventCategoryRepository;

		public IEventRepository EventRepository => _eventRepository;

		public IFeedbackRepository FeedbackRepository => _feedbackRepository;

		public INotificationRepository NotificationRepository => _notificationRepository;

		public IPaymentRepository PaymentRepository => _paymentRepository;

		public IPromotionRepository PromotionRepository => _promotionRepository;

		public ITicketRepository TicketRepository => _ticketRepository;

		

		
	}
}
