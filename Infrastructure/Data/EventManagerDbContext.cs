﻿using Domain.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace Infrastructure.Data
{
    public class EventManagerDbContext: IdentityDbContext<AppUser>
    {
        public EventManagerDbContext()
        {
        }
        public EventManagerDbContext(DbContextOptions<EventManagerDbContext> options) : base(options)
        {
        }

        #region DB Sets
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<EventCategory> EventCategories { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Promotion> Promotions { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<EventEntry> EventEntries { get; set; }

        public DbSet<BookingTicket> BookingTickets { get; set; }
        #endregion

        #region Connect DB
        protected override void OnConfiguring(DbContextOptionsBuilder optionsbuilder)
        {
            if (!optionsbuilder.IsConfigured)
            {
                var config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();
                string ConnectionStr = config.GetConnectionString("DefaultDB");

                optionsbuilder.UseSqlServer(ConnectionStr);
            }
        }
        //private string GetConnectionStrings()
        //{
        //    IConfiguration config = new ConfigurationBuilder()
        //     .SetBasePath(Directory.GetCurrentDirectory())
        //    .AddJsonFile("appsettings.json", true, true)
        //    .Build();
        //    return config["ConnectionStrings:DefaultDB"];
        //}
        #endregion

        #region FluentAPIConfig
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            #region ConfigUser
            builder.Entity<AppUser>()
                .HasMany<Booking>(u => u.Bookings)
                .WithOne(b => b.AppUser)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AppUser>()
                .HasMany<Event>(u => u.Events)
                .WithOne(e => e.AppUser)
                .HasForeignKey(e => e.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AppUser>()
                .HasMany<Notification>(u => u.Notifications)
                .WithOne(n => n.AppUser)
                .HasForeignKey(n => n.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AppUser>()
                .HasMany<Feedback>(u => u.Feedbacks)
                .WithOne(f => f.AppUser)
                .HasForeignKey(f => f.UserId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion

            #region ConfigEventCategory
            builder.Entity<EventCategory>()
                .HasMany<Event>(ec => ec.Events)
                .WithOne(e => e.EventCategory)
                .HasForeignKey(e => e.EventCategoryId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region ConfigEvent
            builder.Entity<Event>()
                .HasMany<Ticket>(e => e.Tickets)
                .WithOne(t => t.Event)
                .HasForeignKey(t => t.EventId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Event>()
                .HasMany<Feedback>(e => e.Feedbacks)
                .WithOne(f => f.Event)
                .HasForeignKey(f => f.EventId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Event>()
                .HasMany<Promotion>(e => e.Promotions)
                .WithOne(t => t.Event)
                .HasForeignKey(t => t.EventId)
                .OnDelete(DeleteBehavior.Restrict);

            #endregion

            #region ConfigBooking
            

            builder.Entity<Booking>()
                .HasOne<Payment>(b => b.Payment)
                .WithOne(p => p.Booking)
                .HasForeignKey<Payment>(p => p.BookingId);
            #endregion

            #region ConfiBookingTicket
            builder.Entity<BookingTicket>().HasKey(bt => new { bt.TicketId, bt.BookingId });
            builder.Entity<BookingTicket>()
                .HasOne<Booking>(bt => bt.Booking)
                .WithMany(b => b.BookingTickets)
                .HasForeignKey(bt => bt.BookingId);

			builder.Entity<BookingTicket>()
				.HasOne<Ticket>(bt => bt.Ticket)
				.WithMany(t=> t.BookingTickets)
				.HasForeignKey(bt => bt.TicketId);
			#endregion


			builder.Entity<EventCategory>().HasData(
            new EventCategory
            {
                Id = Guid.NewGuid(),
                CategoryName = "Sport",
                CategoryDescription = "Interesting sports events.",
                CreatedDate = DateTime.Now,
                UpdatedDate = null
            },
            new EventCategory
            {
                Id = Guid.NewGuid(),
                CategoryName = "Music",
                CategoryDescription = "Exciting music events.",
                CreatedDate = DateTime.Now,
                UpdatedDate = null
            },
            new EventCategory
            {
                Id = Guid.NewGuid(),
                CategoryName = "Art",
                CategoryDescription = "Inspiring art events.",
                CreatedDate = DateTime.Now,
                UpdatedDate = null
            },
            new EventCategory
            {
                Id = Guid.NewGuid(),
                CategoryName = "Technology",
                CategoryDescription = "Latest technology events.",
                CreatedDate = DateTime.Now,
                UpdatedDate = null
            },
            new EventCategory
            {
                Id = Guid.NewGuid(),
                CategoryName = "Education",
                CategoryDescription = "Informative educational events.",
                CreatedDate = DateTime.Now,
                UpdatedDate = null
            },
            new EventCategory
            {
                Id = Guid.NewGuid(),
                CategoryName = "Business",
                CategoryDescription = "Important business events.",
                CreatedDate = DateTime.Now,
                UpdatedDate = null
            }
        );
         
        }
        #endregion
    }
}
