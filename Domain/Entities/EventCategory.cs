﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public partial class EventCategory : BaseEntity
    {
        public EventCategory()
        {
            Events = new HashSet<Event>();
        }
        #region Properties
        [Required]
        public string CategoryName { get; set; }
        public string? CategoryDescription { get; set; }
        #endregion

        #region DB Relationship
        public virtual ICollection<Event> Events { get; set; }
        #endregion
    }
}
