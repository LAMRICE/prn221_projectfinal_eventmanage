﻿    namespace Domain.Entities
{
    public partial class Feedback : BaseEntity
    {


        #region Properties
        public string FeedbackContent { get; set; }
        public DateTime? FeedbackTime { get; set; }
        public string? UserId { get; set; }
        public Guid? EventId { get; set; }
        #endregion

        #region DB Relationship
        public virtual AppUser AppUser { get; set; }
        public virtual Event Event { get; set; }

        #endregion
    }
}
