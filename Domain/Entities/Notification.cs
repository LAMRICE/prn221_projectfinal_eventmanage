﻿namespace Domain.Entities
{
    public partial class Notification:BaseEntity
    {
        #region Properties
        public string NotificationContent { get; set; }
        public DateTime? NotificationTime { get; set; }
        public string? UserId { get; set; }
        #endregion

        #region DB Relationship
        public virtual AppUser AppUser { get; set; }
        #endregion
    }
}
