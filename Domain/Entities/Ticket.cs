﻿namespace Domain.Entities
{
    public partial class Ticket :BaseEntity
    {
        #region Properties
        public string TicketType { get; set; }
        public double TickerPrice { get; set; }
        public int TicketQuantity { get; set; }

        public Guid? EventId { get; set; }
        #endregion

        #region DB Relationship
        public virtual Event Event { get; set; }

        public virtual ICollection<BookingTicket> BookingTickets { get; set; }
        #endregion
    }
}
