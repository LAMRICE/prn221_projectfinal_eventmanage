﻿using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public partial class Event : BaseEntity
    {
        public Event()
        {
            Tickets = new HashSet<Ticket>();
            Feedbacks = new HashSet<Feedback>();
            Promotions = new HashSet<Promotion>();
        }
        #region Properties
        [Required(ErrorMessage = "EventName is required!")]
        public string EventName { get; set; }
        [Required]
        public string EventLocation { get; set; }
        public string UrlImage { get; set; }    
        public DateTime? DateBegin { get; set; }
        public DateTime? DateEnd { get; set; }
        public bool IsDelete { get; set; } = false;
        public string EventDescription { get; set; }
        public string? UserId { get; set; }
        public Guid? EventCategoryId { get; set; }

        #endregion

        #region DB Relationship
        public virtual AppUser AppUser { get; set; }
        public virtual EventCategory EventCategory { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<Promotion>? Promotions { get; set; }

        #endregion
    }
}
