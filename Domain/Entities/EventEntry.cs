﻿namespace Domain.Entities
{
    public partial  class EventEntry : BaseEntity
    {
        #region Properties
        public DateTime EntryTime { get; set; }
        #endregion

        #region DB Relationship
        #endregion
    }
}
