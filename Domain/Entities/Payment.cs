﻿namespace Domain.Entities
{
    public partial class Payment: BaseEntity
    {
        #region Properties
        public string PaymentMethod { get; set; }
        public double PaymentAmount { get; set; }
        public DateTime? PaymentTime { get; set; }
        public Guid? BookingId { get; set; }
        #endregion
        #region DB Relationship
        public Booking? Booking { get; set; }

        #endregion
    }
}
