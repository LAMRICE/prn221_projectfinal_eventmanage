﻿using Microsoft.AspNetCore.Identity;

namespace Domain.Entities
{
    public partial class AppUser : IdentityUser 
    {
        public AppUser() { 
            Bookings = new HashSet<Booking>();
            Events = new HashSet<Event>();
            Notifications = new HashSet<Notification>();
            Feedbacks = new HashSet<Feedback>();
        }
        private const string DefaultAvatar = "https://cdn.sforum.vn/sforum/wp-content/uploads/2023/10/avatar-trang-4.jpg";
        private string? _avatar = DefaultAvatar;
        #region Properties
        public string? LastName { get; set; }
        public string? FirstName {  get; set; }
        public string? Avatar { get => _avatar??=DefaultAvatar; set => _avatar=value; }
        public bool? IsBanned { get; set; } = false;
        #endregion
        public string FullName => FirstName + " " +  LastName;

        #region DB Relationship
        public virtual ICollection<Booking> Bookings { get; set; }
        public virtual ICollection<Event> Events { get; set; }
        public virtual ICollection<Notification> Notifications { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        #endregion
    }
}
