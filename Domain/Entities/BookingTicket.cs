﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
	public partial class BookingTicket
	{
		public Guid BookingId { get; set; }
		public Booking Booking { get; set; }
		public int TicketQuantity { get; set; }
		public Guid TicketId { get; set; }
		public Ticket Ticket { get; set; }
	}
}
