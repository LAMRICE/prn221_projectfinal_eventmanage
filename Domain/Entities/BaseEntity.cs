﻿namespace Domain.Entities
{
    public partial class BaseEntity
    {
        private DateTime? _updateDate;
        #region Properties
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.Now;
        public DateTime? UpdatedDate { get
            {
                if (_updateDate == null) return CreatedDate;
                return _updateDate;
            } set => _updateDate = value;}
		#endregion
	}
}
