﻿namespace Domain.Entities
{
    public partial class Booking:BaseEntity
    {
        public Booking()
        {
        }

        #region Properties
        public DateTime? BookingTime { get; set; }
        public string UserId { get; set; }
        #endregion

        #region DB Relationship
        public virtual AppUser AppUser { get; set; }
        public virtual Payment? Payment { get; set; }

        public virtual ICollection<BookingTicket> BookingTickets { get; set; }
        #endregion
    }
}
