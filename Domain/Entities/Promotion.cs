﻿namespace Domain.Entities
{
    public partial class Promotion : BaseEntity
    {
		#region Properties
		public string PromotionChannel { get; set; }
		public string PromotionCode { get; set; } // Mã khuyến mãi
		public int SlotsAvailable { get; set; } // Số lượng slot có thể áp dụng mã khuyến mãi
		public double DiscountPercent { get; set; } // Phần trăm giảm giá

		public Guid? EventId { get; set; }
		#endregion

		#region DB Relationship
		public virtual Event Event { get; set; }
        #endregion
    }
}
